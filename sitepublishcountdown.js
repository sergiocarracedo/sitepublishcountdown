(function ($) {

    Drupal.behaviors.sitepublishcountdown = {
        attach: function (context, settings) {
            $('time').countDown({
                label_dd:         'días',
                label_hh:         'horas',
                label_mm:         'minutos',
                label_ss:         'segundos',
            }).on('time.elapsed', function() {
                setTimeout(function() {
                    location.reload();
                }, 2000);
            });
        }
    };
    /*
    $(function() {

        $('#countdown').countDown({
            label_dd:         'días',
            label_hh:         'horas',
            label_mm:         'minutos',
            label_ss:         'segundos',
        }).on('time.elapsed', function() {
            setTimeout(function() {
                location.reload();
            }, 2000);
        });
    });*/


    /**/
}(jQuery));